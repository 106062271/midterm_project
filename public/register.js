function init() {
	document.getElementById("register").addEventListener("click", function(){
		var username = document.getElementById("username");
		var email = document.getElementById("email");
		var password = document.getElementById("password");
		firebase.auth().createUserWithEmailAndPassword(email.value,password.value).then(function(){
			var userdata = {
				username: username.value,
				email: email.value
			};
			firebase.database().ref("users/"+firebase.auth().currentUser.uid).set(userdata).then(function(){
				alert("register success");
				firebase.auth().signInWithEmailAndPassword(email.value, password.value).then(function(){
					document.location = "index.html";
				}).catch(function(error){
					alert("signin failed");
					document.location = "signin.html";
					email.value = "";
					password.vaule = "";
				});
			});
		}).catch(function(error){
			alert("register failed: "+error.message);
			email.value = "";
			password.vaule = "";
		});
	});
}
