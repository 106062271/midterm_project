var ID;
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='navbar-brand' href='index.html'>Forum</a><a id ='btn' class='navbar-brand' href='signin.html'>Logout</a><a class='navbar-brand' href='userpage.html'>userpage</a>";
            var btn = document.getElementById('btn');
            btn.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    console.log('Signed Out');
                  }, function(error) {
                    console.error('Sign Out Error', error);
                  });
            })
        } else {
            menu.innerHTML = "<a class='navbar-brand' href='index.html'>Forum</a><a class='navbar-brand' href='signin.html'>Login</a>";
        }
    });

 
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    title = document.getElementById('title');
    var postsRef = firebase.database().ref("commic_title");
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && title.value != "") {
            var user = firebase.auth().currentUser;
            var new_post = {
                uid : user.uid,
                title : title.value,
                comment : post_txt.value
            }
            postsRef.push(new_post);
            post_txt.value = "";
            title.value = "";
        }
    });

    var postsRef = firebase.database().ref('commic_title');

    /*postsRef.once('value')
        .then(function (snapshot) {
            var list = document.getElementById("post_list");
            list.innerHTML = "";
            for(let i in snapshot.val())
            {
                firebase.database().ref('users/'+ snapshot.val()[i].uid).once('value').then(function (snapshot1) {
                    list.innerHTML += '<div class="my-3"><ul class="my-0 text-white-50 " style="word-break:break-all; white-space: pre-wrap">' + snapshot1.child('username').val()+':'+ '</ul>' + '<ul class="my-0 text-white-50 " style="word-break:break-all; white-space: pre-wrap">' +snapshot.val()[i].comment + '</ul></div>';                    
                })
             }
        })
        .catch(e => console.log(e.message));*/
    postsRef.on('value', function (snapshot) {
            var list = document.getElementById("post_list");
            list.innerHTML = "";
            for(let i in snapshot.val())
            {
                list.innerHTML += '<div class="my-3"><ul class=" my-0 text-warning  " style="font-size: 30px ; word-break:break-all; white-space: pre-wrap"><a id="'+i+'" onclick="storeid(this)" href="comic.html">' + snapshot.val()[i].title+ '</a></ul></div>';
            }
    })

}

function storeid(myObj)
{
    ID = myObj.id;
    localStorage.name = ID;
    location.href = 'comic.html';
}