var value = localStorage["name"];
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='navbar-brand' style='word-break:break-all;' href='index.html'>Forum</a><a id ='btn' class='navbar-brand' href='signin.html'>Logout</a><a class='navbar-brand'  href='userpage.html'>userpage</a>";
            var btn = document.getElementById('btn');
            btn.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    console.log('Signed Out');
                  }, function(error) {
                    console.error('Sign Out Error', error);
                  });
            })
        } else {
            menu.innerHTML = "<a class='navbar-brand' href='index.html'>Forum</a><a class='navbar-brand' href='signin.html'>Login</a>";
        }
    });
    firebase.database().ref("fun_title/"+value).once('value').then(  function(shot1){
        firebase.database().ref('users/'+ shot1.val().uid).once('value').then(function (shot2) {
            forum.innerHTML ='<h3 style="word-break:break-all;" class="mb-0 text-danger lh-100">author:'+shot2.val().username+'</h3>'
    })
}) 
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var user = firebase.auth().currentUser;
            var new_post = {
                uid : user.uid,
                comment : post_txt.value
            }
            firebase.database().ref("fun_list/"+value).push(new_post);
            post_txt.value = "";
        }
    });

    var postsRef = firebase.database().ref('fun_list');
    var postcomment = firebase.database().ref('fun_title/'+value);
    postcomment.once('value')
        .then(function (shot) {
            var title = document.getElementById('post_title');
            var comment = document.getElementById('post_com');
            title.innerHTML += '<div class="my-3"><ul><h3 class="my-0 text-danger" style="font-size: 30px ; word-break:break-all; white-space: pre-wrap;">title:'+shot.val().title+'</h3></ul></div>';
            comment.innerHTML += '<div class="my-3"><ul><p class="my-0 text-info " style="padding-left: 30px; font-size: 20px ; word-break:break-all; white-space: pre-wrap;">context:'+shot.val().comment+'</p></ul></div>';
        })
        .catch(e => console.log(e.message));

        firebase.database().ref("fun_list/"+value).on('value', function (snapshot) {
            var list = document.getElementById("post_list");
            list.innerHTML = "";
            for(let i in snapshot.val())
            {
                firebase.database().ref('users/'+ snapshot.val()[i].uid).once('value').then(function (snapshot1) {
                    list.innerHTML += '<div class="my-3"><ul class="my-0 text-warning  " style="font-size: 20px ; word-break:break-all; white-space: pre-wrap;">' + snapshot1.val().username+':'+ '</ul>' + '<ul class="my-0 text-warning  " style="word-break:break-all; white-space: pre-wrap">' +snapshot.val()[i].comment + '</ul></div>';                    
                })
            }
           
    })
}
