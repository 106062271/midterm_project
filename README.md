﻿# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : forum
* Key functions (add/delete)
    1. user page, post page, post list page, leave comment,under any post
    
* Other functions (add/delete)
    Sign Up/In with Google or other third-party accounts 
     • Add Chrome notification 
     • Use CSS animation 
     • Prove your website has strong security (write in your report) 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midproject-c71aa.firebaseapp.com/signin.html]

# Components Description : 
1. [Notification] : [當user登入時，會發出notification。] 

2. [Users' Profile] : [可以更換使用者姓名]
3. [post list] : [可以看該版的所有貼文的標題(post list放四大版標題)]
4. [post] : [可以看到該版的文章以及留言(post放四大板文章以及留言)]
5. [index] : [放四大版連接]
6. [動態css] : [signin 中間上面有個可以翻動的圖片 google.index 有變換顏色的背景]
7. [RWD} : [會隨著視窗大小去調整版面配置。]
8. [論壇list] : [藍色代表title]
9. [title內文] : [紅色代表title 藍色代表內文 黃色代表留言 個人留言最新的會在個人的最上方]

# Other Functions Description(1~10%) : 
1. [post list post 上面的login logout] : [有變換顏色的動態字串，字串根據登入或登出做改變]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
...

## Security Report (Optional)
text input的部分把"<"和">"替換成"&lt","&gt"，這樣就不會有message裡有html code的風險